from aiohttp import web
import json
#import aiohttp
from aiohttp_jinja2 import template
import sqlite3
import subprocess
#from _thread import start_new_thread
import os
import glob
from pathlib import Path
import psutil
#from os import system
from psutil._common import bytes2human

conn = sqlite3.connect("/opt/vms.db")
cur = conn.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS cfg ( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, value TEXT);")
cur.execute("CREATE TABLE IF NOT EXISTS vms ( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, config TEXT, status INTEGER, del INTEGER);")
cur.execute("CREATE TABLE IF NOT EXISTS img ( id INTEGER PRIMARY KEY AUTOINCREMENT, vms INTEGER, file TEXT, format TEXT, size TEXT, cache TEXT, aio TEXT);")
cur.execute("CREATE TABLE IF NOT EXISTS vmcfg ( id INTEGER PRIMARY KEY AUTOINCREMENT, vmid INTEGER, name TEXT, value TEXT);")

def autorun():
    print('Run Autorun!!!')
    cur.execute("SELECT id FROM vms WHERE status='1' and del='0';")
    result = ''
    for id in cur.fetchall():
        print("Run VM " + str(id[0]) + ":")
        res=(subprocess.check_output("sudo systemctl start vms@" + str(id[0]) + " && echo -n True || echo -n Fail", shell=True).strip()).decode()
        result = result + str(id[0]) + ' : ' + res + ';  '
    return result

@template('index.html')
async def index(request):
    site_name = request.app['config'].get('site_name')
    return {'site_name' : site_name}


#@template('api.html')
async def set(request):
    try:
        if request.query['status'] and request.query['id']:
            print('SET' + request.query['id'])
            id=request.query['id']

            if request.query['status'] == 'SNAPSHOT':
                cur.execute("SELECT value FROM vmcfg WHERE name='-snapshot' and vmid = '" + id + "' ;")
                req=cur.fetchall()
                if req[0][0] == 'none':
                    cur.execute("UPDATE vmcfg SET value = ' ' WHERE name='-snapshot' and vmid = '" + id + "' ;")
                    set='Mode shapshot is ON'
                else:
                    cur.execute("UPDATE vmcfg SET value = 'none' WHERE name='-snapshot' and vmid = '" + id + "' ;")
                    set='Mode shapshot is OFF'
                conn.commit()
                print(set)
                response_obj = { 'id' : id , 'act' : set }
                return web.json_response(response_obj, status=200)

            if request.query['status'] == 'CFG':
                cur.execute("UPDATE vms SET status = '1' WHERE id = '" + id + "' ;")
                conn.commit()
                response_obj = { 'id' : id , 'act' : 'Started' }
                return web.json_response(response_obj, status=200)

            if request.query['status'] == 'started':
                cur.execute("UPDATE vms SET status = '1' WHERE id = '" + id + "' ;")
                conn.commit()
                response_obj = { 'id' : id , 'act' : 'Started' }
                return web.json_response(response_obj, status=200)

            if request.query['status'] == 'stopped':
                cur.execute("UPDATE vms SET status = '0' WHERE id = '" + id + "' ;")
                conn.commit()
                response_obj = { 'id' : id , 'act' : 'Stopped' }
                return web.json_response(response_obj, status=200)

    except:
        print('Error SET')

# GET requests
@template('api.html')
async def api(request):
# LIST
    try:
        if request.query['list']:
            if request.query['list'] == 'CFG' and request.query['name']:
                name = request.query['name']
                #print('111')
                cur.execute("SELECT name, value FROM cfg WHERE name='" + name + "' ORDER BY name DESC, value ASC")
                req=cur.fetchall()
                #print(req)
                data = []
                for i in req:
                    dt = { "name" : str(i[0]), "value" : str(i[1]) }
                    data.append(dt)
                response_obj = data
                return web.json_response(response_obj, status=200)

            if request.query['list'] == 'ALLCFG':
                #name = request.query['name']
                #print('111')
                cur.execute("SELECT id, name, value FROM cfg ORDER BY name DESC, value ASC")
                req=cur.fetchall()
                #print(req)
                data = []
                for i in req:
                    dt = { "id" : str(i[0]), "name" : str(i[1]), "value" : str(i[2]) }
                    data.append(dt)
                response_obj = data
                return web.json_response(response_obj, status=200)

            if request.query['list'] == 'IMAGES-FREE':
                #name = request.query['name']
                #print('111')
                cur.execute("SELECT id, file, format, size, cache, aio FROM img WHERE vms = '0'")
                req=cur.fetchall()
                #print(req)
                data = []
                for i in req:
                    dt = { "id" : str(i[0]), "file" : str(i[1]), "format" : str(i[2]), "size" : str(i[3]), "cache" : str(i[4]), "aio" : str(i[5]) }
                    data.append(dt)
                response_obj = data
                return web.json_response(response_obj, status=200)

            if request.query['list'] == 'IMAGES-ALL':
                #name = request.query['name']
                #print('111')
                cur.execute("SELECT id, file, format, size, cache, aio, vms FROM img")
                req=cur.fetchall()
                #print(req)
                data = []
                for i in req:
                    dt = { "id" : str(i[0]), "file" : str(i[1]), "format" : str(i[2]), "size" : str(i[3]), "cache" : str(i[4]), "aio" : str(i[5]), "vms" : str(i[6]) }
                    data.append(dt)
                response_obj = data
                return web.json_response(response_obj, status=200)


            if request.query['list'] == 'VMCFG' and request.query['id']:
                id = request.query['id']
                print('VMCFG')

                cur.execute("SELECT file, format, cache, aio FROM img WHERE vms='" + id + "'")
                req0=cur.fetchall()
                print(req0)
                data1 = []
                for i in req0:
                    cfg0 = { "-drive" : "file=" + str(i[0]) + ",format=" + str(i[1]) + ",cache=" + str(i[2]) + ",aio=" + str(i[3]) }
                    data1.append(cfg0)
                    #print(str(i[0]) + ' ' + str(i[1]))
                    #dts = str(i[0]) + " " + str(i[1])
                    #data1 = data1 + " " + dts
                #print(data1)

                cur.execute("SELECT name, value  FROM vmcfg WHERE vmid='" + id + "'")
                req1=cur.fetchall()
                print(req1)
                #data1 = []
                for i in req1:
                    cfg = { str(i[0]) : str(i[1]) }
                    data1.append(cfg)
                    #print(str(i[0]) + ' ' + str(i[1]))
                    #dts = str(i[0]) + " " + str(i[1])
                    #data1 = data1 + " " + dts
                print(data1)

                cur.execute("SELECT name, config, status, del  FROM vms WHERE id='" + id + "'")
                req=cur.fetchall()
                print(req)
                config = data1
                data = []
                for i in req:
                    #dt = { "name" : str(i[0]), "config" : str(i[1]), "status" : str(i[2]), "del" : str(i[3]) }
                    dt = { "name" : str(i[0]), "config" : config, "status" : str(i[2]), "del" : str(i[3]) }
                    data.append(dt)
                response_obj = data
                return web.json_response(response_obj, status=200)

            if request.query['list'] == 'VMS':
                cur.execute("SELECT id, name, config, status, del FROM vms WHERE del='0'")
                req=cur.fetchall()
                vms = []
                for i in req:
                    cur.execute("SELECT name, value FROM vmcfg WHERE vmid='" + str(i[0]) + "'")
                    req2=cur.fetchall()
                    config=[]
                    for e in req2:
                        config.append({e[0] : e[1]})
                    vm = { "id" : str(i[0]),  "name" :  str(i[1]), "config" : config, "status" : str(i[3]), "del": str(i[4]) }
                    vms.append(vm)
                response_obj = vms
                return web.json_response(response_obj, status=200)

            if request.query['list'] == 'HVStatus':
                list = """
                          psutil.disk_usage('/').total,
                          psutil.disk_usage('/').free,
                          psutil.disk_usage('/').percent,
                          psutil.swap_memory().total,
                          psutil.swap_memory().free,
                          psutil.swap_memory().percent,
                          psutil.virtual_memory().total,
                          psutil.virtual_memory().free,
                          psutil.virtual_memory().percent,
                          psutil.cpu_percent(),
                          psutil.cpu_count(),
                          psutil.cpu_freq().max,
                          psutil.cpu_freq().min,
                          psutil.cpu_freq().current
                       """
                res = []
                for i in list.replace(' ','').replace('\n','').split(','):
                    resl = eval(i)
                    name = i.replace('psutil.','').replace('()','')
                    if name.find('total') != -1 or name.find('free') != -1:
                        value = bytes2human(resl) + 'B'
                    elif name.find('percent') != -1:
                        value = str(resl) + "%"
                    elif name.find('freq.current') != -1:
                        value = str(resl) + " GHz"
                    elif name.find('freq') != -1:
                        value = str(resl/1000) + " GHz"
                    else:
                        value = resl
                    reslt = { 'name' : name, 'value' : value}
                    res.append(reslt)
                #print(res)
                response_obj = res
                return web.json_response(response_obj, status=200)

    except:
        print('Error GET request LIST')

    try:
        if request.query['get']:
            if request.query['get'] == 'autorun':
                result = autorun()
                response_obj = { 'Autorun' : result }
                return web.json_response(response_obj, status=200)

            if request.query['get'] == 'sync':
                cur.execute("SELECT value FROM cfg WHERE name='ISOPATH';")
                req=cur.fetchall()
                for i in req:
                    #print(i)
                    if os.path.isdir(i[0]):
                        list = glob.glob(i[0] + "/*.*")
                        for n in list:
                            print(n)
                            value = n
                            name  = '-cdrom'
                            info = cur.execute("SELECT * FROM cfg WHERE value='" + value + "';")
                            if info.fetchone() is None:
                                print('Add : ' + value)
                                add = (name, value)
                                cur.execute("INSERT INTO cfg (name, value) VALUES(?, ?);", add)
                                conn.commit()
                            else:
                                print('skip')
                            add = (name, value)
                cur.execute("SELECT value, id FROM cfg WHERE name='-cdrom';")
                req=cur.fetchall()
                for i in req:
                    value=i[0]
                    id=i[1]
                    print(value)
                    if value != 'none':
                        if not os.path.isfile(value):
                            print('Not file: ' + value + '... Deleting' + id)
                            cur.execute("DELETE FROM cfg WHERE id='" + id + "' ;")
                            conn.commit()
                response_obj = { 'Sync' : 'OK' }
                return web.json_response(response_obj, status=200)

            if request.query['get'] and request.query['id']:
                act = request.query['get']
                id = request.query['id']
                if act=='start':
                    print('start :: ' + id + ' ::: ' + act)
                    #start_new_thread(subprocess.check_output("service vms@" + id + " " + act + " " , shell=True))
                    result = (subprocess.check_output("service vms@" + id + " " + act + " " , shell=True).strip()).decode()
                    print(result)
                    #cur.execute("UPDATE vms SET status = '1' WHERE id = '" + id + "' ;")
                    #conn.commit()
                    response_obj = { 'id' : id , 'act' : act }
                    return web.json_response(response_obj, status=200)

                if act=='stop':
                    print('stop :: ' + id + ' ::: ' + act)
                    #start_new_thread(subprocess.check_output("service vms@" + id + " " + act + " " , shell=True))
                    result = (subprocess.check_output("service vms@" + id + " " + act + " " , shell=True).strip()).decode()
                    print(result)
                    cur.execute("UPDATE vms SET status = '0' WHERE id = '" + id + "' ;")
                    conn.commit()
                    response_obj = { 'id' : id , 'act' : act }
                    return web.json_response(response_obj, status=200)

    except:
        print('Error GET request GET')

# DELETE
    try:
        if request.query['del']:
            if request.query['del']=='vms' and request.query['id']:
                id = request.query['id']
                try:
                    #cur.execute("DELETE FROM vms WHERE id='" + id + "' ;")
                    result = (subprocess.check_output("service vms@" + id + " stop " , shell=True).strip()).decode()
                    print('Stop VM: ' + result)
                    cur.execute("UPDATE img SET vms = '0' WHERE vms = '" + id + "' ;")
                    cur.execute("UPDATE vms SET del = '1' WHERE id = '" + id + "' ;")
                    response_obj = {"result": "True"}
                    conn.commit()
                    return web.json_response(response_obj, status=200)
                except:
                    response_obj = { "result" : "False"}
                    return web.json_response(response_obj, status=200)

            if request.query['del']=='IMAGE' and request.query['id']:
                id = request.query['id']
                cur.execute("SELECT file FROM img WHERE id='"+ id +"'")
                req=cur.fetchall()
                filename = req[0][0]
                if os.path.isfile(filename):
                    os.remove(filename)
                    print("File " + filename + " deleted!")
                cur.execute("DELETE FROM img WHERE id='" + id + "' AND vms='0';")
                if conn.commit():
                    response_obj = {"result": "True"}
                    return web.json_response(response_obj, status=200)
                else:
                    response_obj = { "result" : "False"}
                    return web.json_response(response_obj, status=200)

            if request.query['del']=='CFG' and request.query['id']:
                id = request.query['id']
                try:
                    cur.execute("DELETE FROM cfg WHERE id='" + id + "' ;")
                    #cur.execute("UPDATE vms SET del = '1' WHERE id = '" + id + "' ;")
                    response_obj = {"result": "True"}
                    conn.commit()
                    return web.json_response(response_obj, status=200)
                except:
                    response_obj = { "result" : "False"}
                    return web.json_response(response_obj, status=200)


    except:
        print('Error GET request DEL')



#POST requests
async def action(request):
    try:
        #print(request.query['add'])
        if request.query['add'] == 'VM':
            data = json.loads(await request.text())
            vmconfig     = data['vmconfig']
            vmname   = data['vmname']
            add = (vmname, vmconfig, 0, 0)
            cur.execute("INSERT INTO vms (name, config, status, del) VALUES(?, ?, ?, ?);", add)
            conn.commit()
            response_obj = { "vmname" : vmname, "vmconfig" : vmconfig }
            return web.json_response(response_obj, status=200)

        if request.query['add'] == 'CREATEVMCFG':
            #print(request.query['add'] + ': OK')
            data = json.loads(await request.text())
            add = ('reserved', 'reserved', 0, 0)
            cur.execute("INSERT INTO vms (name, config, status, del) VALUES(?, ?, ?, ?);", add)
            vmid = cur.lastrowid
            #print(vmid)
            for i in data:
                name=i['name']
                value=i['value']
                #print(name + ' : ' + value)
                add = (vmid, name, value)
                cur.execute("INSERT INTO vmcfg (vmid, name, value) VALUES(?, ?, ?);", add)
            conn.commit()
            response_obj = { "result" : True, "vmid" : vmid }
            return web.json_response(response_obj, status=200)

        if request.query['add'] == 'IMGVM':
            #print(request.query['add'] + ': OK')
            data = json.loads(await request.text())
            vmid = data['vmid']
            imgid = data['imgid']
            cur.execute("UPDATE img SET name = '" + name + "', config = '' WHERE id = '" + id + "' ;")
            conn.commit()
            #add = ('reserved', 'reserved', 0, 0)
            #cur.execute("INSERT INTO vms (name, config, status, del) VALUES(?, ?, ?, ?);", add)
            #conn.commit()
            response_obj = { "result" : True, "create" : id }
            return web.json_response(response_obj, status=200)

        if request.query['add'] == 'CREATEVM':
            #print(request.query['add'] + ': OK')
            data = json.loads(await request.text())
            id = data['id']
            name = data['name']
            imgid = data['imageid'].replace(' ','').split(',')
            for iid in imgid:
                print(iid)
                cur.execute("UPDATE img SET vms = '" + id + "' WHERE id = '" + iid + "' ;")
            cur.execute("UPDATE vms SET name = '" + name + "', config = '' WHERE id = '" + id + "' ;")
            conn.commit()
            #add = ('reserved', 'reserved', 0, 0)
            #cur.execute("INSERT INTO vms (name, config, status, del) VALUES(?, ?, ?, ?);", add)
            #conn.commit()
            response_obj = { "result" : True, "create" : id }
            return web.json_response(response_obj, status=200)

        if request.query['add'] == 'CFG':
            data = json.loads(await request.text())
            value     = data['value']
            name   = data['name']
            add = (name, value)
            cur.execute("INSERT INTO cfg (name, value) VALUES(?, ?);", add)
            conn.commit()
            response_obj = { "cfgname" : name, "cfgvalue" : value }
            return web.json_response(response_obj, status=200)

        if request.query['add'] == 'HDD':
            data = json.loads(await request.text())
            for i in data['obj']:
                #print(i['name'] + ' : ' + i['value'])
                name=i['name']
                value=i['value']
                print(name + ' : ' + value)
                globals()[name] = value
            print(str(format))
            if not os.path.exists(VMSPATH):
                os.makedirs(VMSPATH)
                print('Created folder: ' + VMSPATH )
            add = (file, format, size, cache, aio, '0')
            cur.execute("INSERT INTO img ( file, format, size, cache, aio, vms) VALUES(?, ?, ?, ?, ?, ?);", add)
            id = str(cur.lastrowid)
            #print(id)
            #print(VMSPATH + '/' + str(id) + '_' + file + '.' + format)
            pathfile = VMSPATH + '/' + id + '_' + file + '.' + format
            print(pathfile)
            result = (subprocess.check_output("qemu-img create -f " + format + " " + pathfile + " " + size + " ", shell=True).strip()).decode()
            print(result)
            cur.execute("UPDATE img SET file = '" + pathfile + "' WHERE id = '" + id + "' ;")
            conn.commit()
            response_obj = { 'file' : file, 'format' : format, 'size' : size, 'cache' : cache, 'aio' : aio }
            return web.json_response(response_obj, status=200)


        if request.query['add'] == 'update':
            data = json.loads(await request.text())
            vmid=data['vmid']
            name=data['name']
            value=data['value']
            oldvalue=data['oldvalue']
            #print(id + ' ===  ' + name + ' ---- ' + value + ' : ' + oldvalue)
            cur.execute("SELECT id FROM vmcfg WHERE value = '" + value + "' and vmid='" + vmid + "' and name='" + name + "' ;")
            req2=cur.fetchall()
            id=str(req2[0][0])
            cur.execute("UPDATE vmcfg SET value = '"+ oldvalue +"' WHERE id = '" + id + "';")
            conn.commit()
            response_obj = { 'vmid' : vmid , 'act' : 'updated' }
            return web.json_response(response_obj, status=200)

        if request.query['add'] == 'savevmname':
            data = json.loads(await request.text())
            id=data['id']
            name=data['name']
            cur.execute("UPDATE vms SET name = '"+ name +"' WHERE id = '" + id + "';")
            conn.commit()
            response_obj = { 'id' : id , 'act' : 'updated' }
            return web.json_response(response_obj, status=200)

    except:
        print('Error POST request')
        return aiohttp.web.Response(text='Error', status=200)

#@router.post("/upload")
async def upload(request):
    print('Upload')
    post = await request.post()
    print(post.get("filename"))
    print(request.query["name"])
    #post_id = request.match_info["post"]
    #db = request.config_dict["DB"]
    #post = await request.post()
    #image = post.get("image")
    #await db.execute(
    #    f"UPDATE posts SET title = ?, text = ? WHERE id = ?",
    #    [post["title"], post["text"], post_id],
    #)
    #if image:
    #    img_content = image.file.read()  # type: ignore
    #    await apply_image(db, post_id, img_content)
    #await db.commit()
    #raise web.HTTPSeeOther(location=f"/{post_id}/edit")
