from .views import frontend
import os

def setup_routes(app):
    STATIC_PATH = os.path.join(os.path.dirname(__file__), "static")
    app.router.add_static('/static/', STATIC_PATH, name='static')
    app.router.add_route('GET', '/', frontend.index)
    app.router.add_route('POST', '/api', frontend.action)
    app.router.add_route('GET', '/api', frontend.api)
    #app.router.add_route('POST', '/upload', frontend.upload)
    app.router.add_route('GET', '/api/set', frontend.set)
