#!/bin/bash

#ABSOLUTE_FILENAME=`readlink -e "$0"`
#PTH=`dirname "$ABSOLUTE_FILENAME"`
PTH=`curl -s "http://127.0.0.1/api?list=CFG&name=VMSPATH" | jq .[].value -r`

if [[ -z $2 ]]; then
	echo "Нет параметров "
	exit;
fi

ACT=$1
ID=$2
#MAC=$( printf "%02d" `echo "obase=16; ${ID} " | bc`)
MAC=`echo 0$(echo "obase=16; ${ID} " | bc) | grep -o ..$`

startvm () {
    if [[ $ACT == 'start' ]]
    then
        #echo Start $ID
        REQ=`curl -s "http://127.0.0.1:8080/api?list=VMCFG&id=${ID}"`
	#REQ=`wget -qO - "http://127.0.0.1:8080/api?list=VMCFG&id=${ID}"`
	#CONFIG=`echo $REQ | jq .[] | jq -r .config | sed "s|#ID#|${MAC}|g"`
	DEL=`echo $REQ | jq .[] | jq -r .del`
	if [[ $DEL == '0' ]]
        then
            #ISO=`echo $CONFIG | sed 's|.* -cdrom ||' | awk {'print $1'}`
            #IMG=`echo $CONFIG | sed 's|.*file=||' | tr ',' ' ' | awk {'print $1'}`
            #FRM=`echo $CONFIG | sed 's|.*format=||' | tr ',' ' ' | awk {'print $1'}`
            #test -f $ISO || echo Not found $ISO
            #test -d $PTH/VMS/$ID || mkdir -p $PTH/VMS/$ID
            #test -f $PTH/VMS/$ID/$IMG || qemu-img create -f $FRM $PTH/VMS/$ID/$IMG 10G
            #cd $PTH/VMS/$ID/
            CONFIG=`echo "$REQ" | jq -r .[].config | jq -c .[] | sed 's| |###|g'`
            VMNAME=`echo "$REQ" | jq -r .[].name | sed 's| | |g'`
            LINE=''

            for i in $CONFIG
            do
                NAME=`echo $i | jq -c | sed 's|"| |g' | awk '{print $2}'`
                VALUE=`echo $i | jq -c | sed 's|"| |g' | awk '{print $4}' | sed 's|###| |g'`
                #echo $NAME $VALUE
                #LINE=`echo $LINE $NAME $VALUE`

                if [[ $VALUE != 'none' ]]
                then
                    if [[ $NAME == '-cdrom' ]]
                    then
                        LINE=`echo $LINE -drive format=raw,media=cdrom,readonly,file="$VALUE"`
                    else
                        LINE=`echo $LINE $NAME $VALUE`
                    fi
                else
                    echo 'Skip'
                fi

                if [[ $NAME == '-cdrom' ]]
                then
                    #echo -n "It's CDROM!"
                    FILE=`echo "$VALUE"`
                    #echo " $FILE"
                    test -f "$FILE" && echo "$FILE - OK!" || ( echo "Not found $FILE"; exit )
                fi

                if [[ $NAME == '-drive' ]]
                then
                    #echo -n "It's DRIVE!"
                    FORMAT=`echo "$VALUE" | sed 's|,|\n|g' | grep '^format=' | sed 's|^format=||g'`
                    FILE=`echo "$VALUE" | sed 's|,|\n|g' | grep '^file=' | sed 's|^file=||g'`
                    #echo " $FILE $FORMAT"
                    test -f "$FILE" && echo "$FILE - OK!" || exit
                fi
            done
            #echo *****************
            #echo $LINE


            CONFIG=`echo $LINE | sed "s|#ID#|${MAC}|g"`


            echo qemu-system-x86_64 -display none -enable-kvm -name "$VMNAME" $CONFIG
            #exit
            #( nohup  qemu-system-x86_64 $CONFIG -vnc 127.0.0.1:$ID; curl -s "http://127.0.0.1:8080/api/set?status=stopped&id=${ID}" ) & echo $!
            # curl -s "http://127.0.0.1:8080/api/set?status=stopped&id=${ID}"
	    if ( nohup qemu-system-x86_64 -display none -enable-kvm -name "$VMNAME" $CONFIG -vnc 127.0.0.1:$ID; cat nohup.out; curl -s "http://127.0.0.1:8080/api/set?status=stopped&id=${ID}"; systemctl stop vms@${ID} & ) &
            then
                #echo $!
                #echo Started!
                curl -s "http://127.0.0.1:8080/api/set?status=started&id=${ID}"
                exit 0
            #else
            #    echo Error! not started...
            #    exit 1
            fi
	else
	    echo ERROR
            exit 1
	fi
    fi
}

startvm

if [[ $ACT == 'stop' ]]; then
        echo stop $ID
        curl -s "http://127.0.0.1:8080/api/set?status=stopped&id=${ID}" | jq .[]
        echo qemu-system-x86_64 $CONFIG
fi
