import argparse
import asyncio
import aiohttp
from demo import create_app
from demo.settings import load_config
from demo.views.frontend import autorun
#from multiprocessing import Process

try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    print("Library uvloop not found")

parser = argparse.ArgumentParser(description='QEmu WEB Manager')
parser.add_argument("-c", "--config", type=argparse.FileType('r'),
    help="Path to config file"
)
args = parser.parse_args()

app = create_app(config=load_config(args.config))

if __name__ == '__main__':
    aiohttp.web.run_app(app, )
