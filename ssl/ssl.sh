#!/bin/bash
openssl genrsa -out rootCA.key 2048
openssl req -x509 -new -key rootCA.key -days 365 -out rootCA.crt

openssl genrsa -out vms.ru.key 2048
openssl req -new -key vms.ru.key -config cert.ini -out vms.ru.csr
openssl x509 -req -in vms.ru.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out vms.ru.crt -days 365 -extensions v3_req -extfile cert.ini
